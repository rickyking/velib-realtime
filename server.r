



shinyServer(function(input, output, session) {

	map <- createLeafletMap(session, "map")

	stationsInBounds <- reactive({
		if (is.null(input$map_bounds))
			return(stations()[FALSE,])
		bounds <- input$map_bounds
    	latRng <- range(bounds$north, bounds$south)
    	lngRng <- range(bounds$east, bounds$west)

    	subset(stations(), position$lat >= latRng[1] & position$lat <= latRng[2] &
    		position$lng >= lngRng[1] & position$lng <=lngRng[2])
	})

	stations <- reactive({
		input$update
		stations <- getStations(key=key)
		stations["ratio_v"] <- stations$available_bikes / (stations$available_bikes + stations$available_bike_stands)
		stations["ratio_d"] <- stations$available_bike_stands / (stations$available_bikes + stations$available_bike_stands)
		stations$ratio_v[is.na(stations$ratio_v)] <- 0
		stations$ratio_d[is.na(stations$ratio_d)] <- 0
        stations["ratio_v_cat"] <- cut(stations$ratio_v, 5)
		stations["ratio_d_cat"] <- cut(stations$ratio_d, 5)
		return(stations)
	})

	session$onFlushed(once=TRUE, function() {

		paintObs <- observe({
			dat <- stations()
			# clear existing shape and draw
			map$clearShapes()
			if (input$velibDock == "v") {
				colors <- brewer.pal(5, 'RdYlGn')[cut(dat$ratio_v, 5, labels=F)]
                ind <- dat$status=="CLOSED"
                colors[ind] <- "#A4A4A4"
				map$addCircle(
					dat$position$lat, dat$position$lng,
					10, dat$number,
					eachOptions=list(color = colors)
				)
			} else {
				colors <- brewer.pal(5, 'RdYlGn')[cut(dat$ratio_d, 5, labels=F)]
				ind <- dat$status=="CLOSED"
				colors[ind] <- "#A4A4A4"
				map$addCircle(
					dat$position$lat, dat$position$lng, 
					10, dat$number,
					eachOptions = list(color = colors))				
			}
		})
		# TIL this is necessary in order to prevent the observer from
		# attempting to write to the websocket after the session is gone.
		session$onSessionEnded(paintObs$suspend)
	})

	# Show a popup at the given location
	showStationPopup <- function(number, lat, lng) {
	  selectedStation <- stations()[stations()$number == number,]
	  content <- as.character(tagList(
		tags$h4("Station: ", selectedStation$name),
		tags$strong(HTML(sprintf("%s",
		  selectedStation$address
		))), tags$br(),
		sprintf("The station is: %s", selectedStation$status), tags$br(),
		sprintf("Velib available: %s", selectedStation$available_bikes), tags$br(),
		sprintf("Dock available: %s", selectedStation$available_bike_stands), tags$br(),
		sprintf("Last update: %s", selectedStation$last_update), br(),
		sprintf("Dock(s) require(s) repair: %s", selectedStation$bike_stands - selectedStation$available_bikes - selectedStation$available_bike_stands)
	  ))
	  map$showPopup(lat, lng, content, number)
	}

	  # When map is clicked, show a popup with station info
	selected <- numeric()
	clickObs <- observe({
	  map$clearPopups()
	  event <- input$map_shape_click
	  if (is.null(event))
		return()
	  
	  isolate({
		showStationPopup(event$id, event$lat, event$lng)
		selected <<- as.character(event$id)
	  })
	})
	
	session$onSessionEnded(clickObs$suspend)

	# test of reactive click
# 	output$test_selected <- renderText({
# 		event <- input$map_shape_click
# 		print(as.character(selected))
# 	})

	# Search with Metro Station
	metroimage <- session$registerDataObj(
		name   = 'ratpMetro', # an arbitrary but unique name for the data object
		data   = metroStopCoord,
		filter = function(data, req) {
			query <- parseQueryString(req$QUERY_STRING)
			metro <- query$stat  # metro name
			selectedMetro <- unique(data[which(data$stop_name == metro),".id"])  
			# get the metro image
			## for fine tuning of multi-metro-image, wait for next version
			# !!!!!!!!!!!!!!!!!! NEED REPROGRAMMiNG!!!!!!!!!!!!!!!!!
			# Potential solution : http://cran.r-project.org/web/packages/png/png.pdf
			image <- switch(selectedMetro[1], 
				"METRO_1" = "www/images/Paris_m_1_jms.svg.png",
				"METRO_2" = "www/images/Paris_m_2_jms.svg.png",
				"METRO_3" = "www/images/Paris_m_3_jms.svg.png",
				"METRO_3b" = "www/images/Paris_m_3bis_jms.svg.png",
				"METRO_4" = "www/images/Paris_m_4_jms.svg.png",
				"METRO_5" = "www/images/Paris_m_5_jms.svg.png",
				"METRO_6" = "www/images/Paris_m_6_jms.svg.png",
				"METRO_7" = "www/images/Paris_m_7_jms.svg.png",
				"METRO_7b" = "www/images/Paris_m_7bis_jms.svg.png",
				"METRO_8" = "www/images/Paris_m_8_jms.svg.png",
				"METRO_9" = "www/images/Paris_m_9_jms.svg.png",
				"METRO_10" = "www/images/Paris_m_10_jms.svg.png",
				"METRO_11" = "www/images/Paris_m_11_jms.svg.png",
				"METRO_12" = "www/images/Paris_m_12_jms.svg.png",
				"METRO_13" = "www/images/Paris_m_13_jms.svg.png",
				"METRO_14" = "www/images/Paris_m_14_jms.svg.png",
				"METRO_Fun" = "www/images/Metro-M.svg.png",
				"METRO_Orv" = "www/images/Metro-M.svg.png",
				)
			shiny:::httpResponse(
				200, 'image/png', readBin(image, 'raw', file.info(image)[, 'size'])
	 		)
		}
	)

	# update the render function for selectize
	updateSelectizeInput(
		session, 'metroStation', server = TRUE,
		choices = metroStopUnique$stop_name,
		options = list(render = I(sprintf(
		  "{
			  option: function(item, escape) {
				return '<div><img width=\"25\" height=\"25\" ' +
					'src=\"%s&stat=' + escape(item.value) + '\" />' +
					escape(item.value) + '</div>';
			  }
		  }",
			metroimage
		)))
	)

	# Reactive Metro View
	metroSelectedObs <- observe({
		selected <- input$metroStation
		latlon <- unique(metroStopCoord[metroStopCoord$stop_name == selected,c("stop_lat", "stop_lon")])
		map$setView(latlon$stop_lat[1], latlon$stop_lon[1], zoom = 16, forceReset = TRUE)
	})

    output$historyTuning <- renderUI({
        event <- input$map_shape_click
        if (is.null(event)) {
            return()
        } else {
            return(list(h4("Station Historical Evolution"),
                dateRangeInput("historyDate", "Select Date Range:", 
                               start = NULL, end = NULL, min = "2014-06-10", 
                               max = Sys.Date(), format = "yyyy-mm-dd", 
                               startview = "month", weekstart = 0, language = "en", 
                               separator = " TO "),
                checkboxInput("predictEnabled", "Enable Prediction?", value = FALSE)
                ))
        }
        
    })
    
#     output$historicalPlotClassic <- renderPlot({
#         event <- input$map_shape_click
#         plotWhat <- input$velibDock # v for velib, d for dock
#         dateRange <- input$historyDate
#         if (is.null(event))
#             return()
# 
#         isolate({
#             station_id <- event$id
#             if (is.null(dateRange)) return()
#             dat <- getStationHist(station_id, dateRange)
#             if (plotWhat == "v") {
#                 plot(available_bikes ~ last_update, data=dat, type = "l", 
#                      xlab="Hour",ylab="Numbers of Velib",
#                      # xaxt = "n",
#                      main = "Historical Evolution"
#                 )
#                 require(lubridate)
#                 start <- dat$last_update[1]
#                 start <- start + dminutes(10)
#                 end <- dat$last_update[length(dat$last_update)]
#                 # axis.POSIXct(1, at=seq(start, end, by = "hour"), format = "%H", las=2)
#             } else {
#                 plot(available_bike_stands ~ last_update, data=dat, type = "l", 
#                      xlab="Hour",ylab="Numbers of Docks",
#                      # xaxt = "n",
#                      main = "Historical Evolution"
#                 )
#                 require(lubridate)
#                 start <- dat$last_update[1]
#                 start <- start + dminutes(10)
#                 end <- dat$last_update[length(dat$last_update)]
#                 # axis.POSIXct(1, at=seq(start, end, by = "hour"), format = "%H", las=2)
#             }
#         })
#     })
    
    historicalData <- reactive({
        event <- input$map_shape_click
        dateRange <- input$historyDate
        if (is.null(event))
            return()
        isolate({
            station_id <- event$id
            if (is.null(dateRange)) return()
            db <- "velib"
            predict_dateRange <- c(as.Date("2014/06/10"), today())
            dat <- getStationHist(station_id, predict_dateRange)      
            predict_res_velib <- predict_velib(unique(dat))
            predict_res_dock <- predict_dock(unique(dat))
            predict_res <- merge(predict_res_velib, predict_res_dock, by = "date")
            names(predict_res) <- c("last_update", "available_bikes", "available_bike_stands")
            predict_res <- subset(predict_res, last_update > now())
            predict_res$available_bikes <- round(predict_res$available_bikes)
            predict_res$available_bike_stands <- round(predict_res$available_bike_stands)
            NFP_dat <- subset(dat, as.Date(last_update) >= dateRange[1] & 
                       as.Date(last_update) <= dateRange[2])
            Predict_dat <- subset(dat, as.Date(last_update) >= dateRange[1], select = c("available_bike_stands", "available_bikes", "last_update"))
            Predict_dat <- rbind(Predict_dat, predict_res)
            return(list(NFP = NFP_dat, FP = Predict_dat))
        })
    })

	output$historicalPlotInteractive <- renderChart({
	    event <- input$map_shape_click
	    if (is.null(event))
	        return()
	    dateRange <- input$historyDate
	    plotWhat <- input$velibDock # v for velib, d for dock
	    predictOrNot <- input$predictEnabled
	    
	    isolate({
            if (predictOrNot == TRUE) {
                dat <- historicalData()$FP
            } else {
                dat <- historicalData()$NFP
            }
	        
	        if (plotWhat == "v") {
	            plotData <- unique(dat[, c("last_update", "available_bikes")])
	            plotData$last_update <- as.numeric(plotData$last_update+2*3600)*1000
	            plot <- rCharts::Highcharts$new()
                plot$chart(type="area", height = 300, zoomType = 'x')
                data <- list()
                for (i in 1:dim(plotData)[1]) {
                    data[[i]] <- list(plotData$last_update[i], plotData$available_bikes[i])
                }
	            plot$series(data = data, name = "Available Bikes")
	            # m1 <- hPlot(x = "last_update", y = "available_bikes", type = "area", data = plotData, title="Realtime Evolution")
	            plot$yAxis(title = list(text = "Nb of Vélib"))
                plot$addParams(dom="historicalPlotInteractive")
                plot$plotOptions(area=list(marker = list(enabled=FALSE, radius=2, states=list(hover=list(enabled=TRUE)))), areaspline = list(fillOpacity = 0.5))
	            plot$legend(enabled=FALSE)
	            if (predictOrNot == TRUE) {
	                plot$xAxis(type= 'datetime', plotBands=list(c(from = as.numeric(now()+2*3600)*1000, to = as.numeric(now()+27*3600)*1000, color = "rgba(68, 170, 213, .2)")))
	                plot$plotOptions(area=list(marker = list(enabled=FALSE, radius=2, states=list(hover=list(enabled=TRUE)))), areaspline = list(fillOpacity = 0.5))
                } else {
                    plot$xAxis(type= 'datetime')
                    plot$plotOptions(area=list(marker = list(enabled=FALSE, radius=2, states=list(hover=list(enabled=TRUE)))))
	            }
                return(plot)
	        } else {
	            plotData <- unique(dat[, c("last_update", "available_bike_stands")])
	            plotData$last_update <- as.numeric(plotData$last_update+2*3600)*1000
	            plot <- rCharts::Highcharts$new()
	            plot$chart(type="area", height = 300, zoomType = 'x')
	            data <- list()
	            for (i in 1:dim(plotData)[1]) {
	                data[[i]] <- list(plotData$last_update[i], plotData$available_bike_stands[i])
	            }
	            plot$series(data = data, name = "Available Bikes")
	            # m1 <- hPlot(x = "last_update", y = "available_bike_stands", type = "area", data = plotData, title="Realtime Evolution")
	            plot$yAxis(title = list(text = "Nb of Docks"))
	            plot$addParams(dom="historicalPlotInteractive")
	            plot$legend(enabled=FALSE)
	            if (predictOrNot == TRUE) {
	                plot$xAxis(type= 'datetime', plotBands=list(c(from = as.numeric(now()+2*3600)*1000, to = as.numeric(now()+27*3600)*1000, color = "rgba(68, 170, 213, .2)")))
	                plot$plotOptions(area=list(marker = list(enabled=FALSE, radius=2, states=list(hover=list(enabled=TRUE)))), areaspline = list(fillOpacity = 0.5))
	            } else {
	                plot$xAxis(type= 'datetime')
	                plot$plotOptions(area=list(marker = list(enabled=FALSE, radius=2, states=list(hover=list(enabled=TRUE)))))
	            }
                return(plot)
	        }
	    })
	})

    output$ui_historicalPlotInteractive <- renderUI({
        event <- input$map_shape_click
        if (is.null(event))
            return()
        return(list(showOutput("historicalPlotInteractive", "highcharts")))
    })
    
	output$globalStat <- renderUI({
		dat <- stations()
		openclosed <- as.data.frame(table(dat$status), stringsAsFactors = FALSE)
		opendat <- dat[dat$status == "open",]

		list(
			tags$ul(
				tags$li(paste0("Opening/Closed stations: ", openclosed[openclosed$Var1=="OPEN", "Freq"], " / ", openclosed[openclosed$Var1=="CLOSED", "Freq"])),
				tags$li()
			)
		)
	})

	output$zoomStat <- renderUI({
		dat <- stationsInBounds()
        if (dim(dat)[1]==0) 
            return()
		openclosed <- as.data.frame(table(dat$status), stringsAsFactors = FALSE)
		opendat <- dat[dat$status == "OPEN",]
        stock_nv <- as.data.frame(table(opendat$ratio_v_cat))
        high_stock <- sum(stock_nv[as.numeric(stock_nv$Var1) %in% c(4,5), "Freq"])
		low_stock <- sum(stock_nv[as.numeric(stock_nv$Var1) %in% c(1,2), "Freq"])
        list(
		    tags$ul(
		        tags$li(paste0("Opening/Closed stations: ", max(openclosed[openclosed$Var1=="OPEN", "Freq"],0), " / ", max(openclosed[openclosed$Var1=="CLOSED", "Freq"], 0))),
		        tags$li(paste0("Stations with High/Low Stock of Vélib: ", max(high_stock,0), " / ", max(low_stock,0)))
		    )
		)
	})

    SAdat <- reactive({
        progress <- Progress$new(session)
        dateRange <- input$SADate
        lowlv <- input$SA_lowlv
        highlv <- input$SA_highlv
        timecr <- input$SA_time
        isolate({
            if (is.null(dateRange)) return()
            df <- list()
            progress$set(message = 'Getting Data...', value = 0.2)
            filename <- paste0("data/velib_data_", paste(format(dateRange, "%Y%m%d"), collapse="_"), "_list.rds")
            if (file.exists(filename)) {
                df_all <- readRDS(filename)
            } else {
            for (i in 1:length(point_data$Number)){
                df[[i]] <- try(getStationHist(point_data$Number[i], dateRange))
            }
            for (i in 1:length(point_data$Number)){
                if(class(df[[i]])== "try-error") df[[i]] <- data.frame()
            }
                progress$set(message = 'Calculating Data...', value = 0.4)
                df_all <- do.call(rbind, df)
                df_all <- unique(df_all)
                df_all <- subset(df_all, status == "OPEN")
                df_all["ratio_v"] <- df_all$available_bikes / (df_all$available_bikes + df_all$available_bike_stands)
                df_all["ratio_d"] <- df_all$available_bike_stands / (df_all$available_bikes + df_all$available_bike_stands)
                df_all$ratio_v[is.na(df_all$ratio_v)] <- 0
                df_all$ratio_d[is.na(df_all$ratio_d)] <- 0
                df_all$hour <- hour(df_all$last_update)
                df_all$day <- format(df_all$last_update, "%Y%m%d")
#             df_all["ratio_v_cat"] <- cut(df_all$ratio_v, 5)
#             df_all["ratio_d_cat"] <- cut(df_all$ratio_d, 5)
                saveRDS(df_all, paste0("data/velib_data_", paste(format(dateRange, "%Y%m%d"), collapse="_"), "_list.rds"))
            }
            df_low <- subset(df_all, ratio_v <= lowlv)
            df_high <- subset(df_all, ratio_v >= highlv)
            progress$set(message = 'Aggregatting Data...', value = 0.7)
            tab_low <- as.data.frame(table(unique(df_low[c("number", "day", "hour")])), stringsAsFactors = F)
            tab_low_hour <- aggregate(tab_low$Freq, tab_low[c("number", "day")], sum)
            tab_low_hour_day <- aggregate(tab_low_hour$x, tab_low_hour[c("number")], mean)
            id_low <- tab_low_hour_day[tab_low_hour_day$x >= timecr, "number"]
            progress$set(message = 'Analyzing Data...', value = 1)
            tab_high <- as.data.frame(table(unique(df_high[c("number", "day", "hour")])), stringsAsFactors = F)
            tab_high_hour <- aggregate(tab_high$Freq, tab_high[c("number", "day")], sum)
            tab_high_hour_day <- aggregate(tab_high_hour$x, tab_high_hour[c("number")], mean)
            id_high <- tab_high_hour_day[tab_high_hour_day$x >= timecr, "number"]
            progress$close()
            return(list(all = df_all, low = id_low, high = id_high))
        })
    })
    
    output$SAstat <- renderUI({
        return(list(
            h4("Statistics"),
            tags$ul(
                tags$li(paste0("Number of Stations in High Level of Stock by definition: ", length(SAdat()$high))),
                tags$li(paste0("Number of Stations in Low Level of Stock by definition: ", length(SAdat()$low)))
            )
            
        ))
    })
    
    output$SAstat_plot <- renderChart({
        dat <- c(length(SAdat()$high), length(SAdat()$low))
        plot <- rCharts::Highcharts$new()
        plot$chart(type = "column", height=250)
        plot$series(data = dat, name = "Nb of stations")
        plot$legend(enabled = F)
        plot$xAxis(categories = c("High Level", "Low Level"))
        plot$yAxis(title = list(text="Nb of stations"))
        plot$addParams(dom = "SAstat_plot")
        return(plot)
    })

    observe({
        if (is.null(input$goto))
            return()
        isolate({
            map$clearPopups()
            dist <- 0.009
            zip <- input$goto$zip
            lat <- input$goto$lat
            lng <- input$goto$lng
            showStationPopup(zip, lat, lng)
            map$fitBounds(lat - dist, lng - dist,
                          lat + dist, lng + dist)
        })
    })


    output$SAtable <- renderDataTable({
        number <- as.numeric(switch(input$SA_hl, "HIGH" = SAdat()$high, "LOW" = SAdat()$low))
        dat <- subset(point_data, Number %in% number)
        dat["Action"] <- paste('<a class="go-map" href="" data-lat="', dat$Latitude, '" data-long="', dat$Longitude, '" data-zip="', dat$Number, '"><i class="fa fa-crosshairs"></i></a>', sep="")
        return(dat)
    })

})