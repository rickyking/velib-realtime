# Velib Realtime Predictor

This demo is a good example of showing realtime API calling, No-SQL connection with R, prediction capability with R, Open data case usage.

## Concept

Get Vélib data from [JCDecaux api]() and save the data in a no-sql database, present the realtime analysis via R-shiny.

## Procedure

- API reading
- No-SQL storage
- Shiny and leaflet
- RATP open data

### Shiny and Leaflet

The shiny application use the example of [superzip](http://shiny.rstudio.com/gallery/superzip-example.html) with package [leaflet-shiny](https://github.com/jcheng5/leaflet-shiny). 

#### Add geocoding search capability

Some minor configurations are required in `leaflet-shiny` packagein order to show the search bar of certain address.

We should modify in the `leaflet-shiny` package the file `www\binding.js`: Line 190 add code: `map.addControl(bingGeocoder);`.

And in the application, `ui.R`:

```R
shinyUI(
    fluidPage(
        div(class="outer",
            tags$head(
            # Include our custom CSS
            includeCSS("styles.css"),
            includeCSS("www/Control.BingGeocoder.css")
            # includeScript("gomap.js")
            ),
            includeScript("www/Control.BingGeocoder.js"),
            includeScript("geocoder.js"),
        ...
```

