var options = {
    collapsed: true, /* Whether its collapsed or not */
    position: 'topleft', /* The position of the control */
    text: 'Locate', /* The text of the submit button */
    callback: function (results) {
        var bbox = results.resourceSets[0].resources[0].bbox,
            first = new L.LatLng(bbox[0], bbox[1]),
            second = new L.LatLng(bbox[2], bbox[3]),
            bounds = new L.LatLngBounds([first, second]);
        this._map.fitBounds(bounds);
    }
};
var bingGeocoder = new L.Control.BingGeocoder("Aq1OdgJVGKWyPuRcaIpgsG8pZcSzy0wLF31OVyY8sSq0sWA5npVn-MAAU9sVU97f", options);
// map.addControl(bingGeocoder);
